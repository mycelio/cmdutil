package logger

import (
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"os"
)

var logger log.Logger

type Options struct {
	Verbosity int
}

func NewLogger(options Options) *log.Logger {
	logger = log.NewLogfmtLogger(os.Stdout)
	level.WarnValue()
	switch options.Verbosity {
	case 3:
		logger = level.NewFilter(logger, level.AllowDebug())
	case 2:
		logger = level.NewFilter(logger, level.AllowInfo())
	case 1:
		logger = level.NewFilter(logger, level.AllowWarn())
	default:
		logger = level.NewFilter(logger, level.AllowError())
	}

	return &logger
}

func Log(keyvals ...interface{}) error {
	return logger.Log(keyvals...)
}

func Debug(keyvals ...interface{}) error {
	return level.Debug(logger).Log(keyvals...)
}

func Info(keyvals ...interface{}) error {
	return level.Info(logger).Log(keyvals...)
}

func Warn(keyvals ...interface{}) error {
	return level.Warn(logger).Log(keyvals...)
}

func Error(keyvals ...interface{}) error {
	return level.Error(logger).Log(keyvals...)
}
