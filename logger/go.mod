module gitlab.com/mycelio/cmdutil/logger

go 1.17

require github.com/go-kit/kit v0.11.0

require github.com/go-logfmt/logfmt v0.5.0 // indirect
