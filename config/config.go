package config

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
)

type Options struct {
	Path            string
	DefaultFileName string
}

func Configure(options Options) {
	var err error

	if options.Path != "" {
		useConfigurationFile(options.Path)
	} else if options.DefaultFileName != ""{
		useDefaultConfigurationFile(options.DefaultFileName)
	} else {
		useDefaultConfigurationFile("config.yaml")
	}

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	cobra.CheckErr(err)
}

func useConfigurationFile(path string)  {
	viper.SetConfigFile(path)
	viper.SetConfigType("yaml")
}

func useDefaultConfigurationFile(fileName string) {
	var err error

	home, err := os.UserHomeDir()
	cobra.CheckErr(err)

	workingDir, err := os.Getwd()
	cobra.CheckErr(err)

	viper.AddConfigPath(home)
	viper.AddConfigPath(workingDir)
	viper.SetConfigType("yaml")
	viper.SetConfigName(fileName)
}
